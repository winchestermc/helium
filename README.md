# Helium

Helium is a grading system for math contests. It was originally developed for HMMT in October of 2016 and since forked for WinMaC.

### SETUP

##### Step 1: Application
Create a Django application, as usual:  
```django-admin startproject WinMaC```

##### Step 2: Clone Helium
Clone the Helium repository into a directory "/helium".  
```cd WinMaC```<br />
```git clone https://gitlab.com/winchestermc/helium.git```<br />

##### Step 3: Add URL's
In `WinMaC/urls.py`, add `url(r'^helium/', include('helium.urls'))` into urlpatterns.
Thus the file should look something like:  
```
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^helium/', include("helium.urls")),
    url(r'^$', RedirectView.as_view(url='helium/')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```

##### Step 4: Add to `INSTALLED_APPS`

In `WinMaC/settings.py`, add `django.templatetags`, `import_export`, and `helium` to installed applications:
```
INSTALLED_APPS = [
    'django.contrib.admin',
    #...
    'django.contrib.staticfiles',
    'django.templatetags',
    'import_export',
    'helium',
]
```

##### Step 5: STATIC and MEDIA settings.

Configure STATIC and MEDIA settings. This is done in `WinMaC/settings.py`.

An example of such a setup might be:
```
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
```

##### Step 6: Install dependencies

Install all required modules:

```pip install django django-import-export pillow pyzbar opencv-python```

##### Step 7: Run migrations

The Helium models need to be migrated into the application:

```python manage.py makemigrations helium```<br />
```python manage.py migrate```<br />

##### Step 8: Create superuser

A superuser must be created to access the Django admin console and Helium backend:

```python manage.py createsuperuser```

##### Step 9: Start Helium

Run Helium as a local server:

```python manage.py runserver```